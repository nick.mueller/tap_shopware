"""s5 tap class."""

from pathlib import Path
from typing import List
import click
from singer_sdk import Tap, Stream
from singer_sdk.typing import (
    ArrayType,
    BooleanType,
    DateTimeType,
    IntegerType,
    NumberType,
    ObjectType,
    PropertiesList,
    Property,
    StringType,
)

# TODO: Import your custom stream types here:
from tap_s5.streams import (
    s5Stream,
    CustomerStream,
    OrdersStream,
    AddressesStream,
    ArticlesStream
)


# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    CustomerStream,
    OrdersStream,
    AddressesStream,
    ArticlesStream
]


class Taps5(Tap):
    """s5 tap class."""

    name = "tap-s5"

    config_jsonschema = PropertiesList(
        Property("username", StringType, required=True),
        Property("password", StringType, required=True),
        Property("start_date", DateTimeType),
        Property("api_url", StringType, required=True),
        Property("use_state", BooleanType, required=True)
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


# CLI Execution:

cli = Taps5.cli
