"""Stream class for tap-s5."""

import requests
import copy
from copy import deepcopy
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable
import base64
import pendulum
from singer_sdk.streams import RESTStream
import datetime
from singer_sdk.authenticators import (
    APIAuthenticatorBase,
    SimpleAuthenticator,
    OAuthAuthenticator,
    OAuthJWTAuthenticator
)

from singer_sdk.typing import (
    ArrayType,
    BooleanType,
    DateTimeType,
    IntegerType,
    NumberType,
    ObjectType,
    PropertiesList,
    Property,
    StringType,
)

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class s5Stream(RESTStream):
    """shopware5 stream class."""
    # def __init__(self, tap):
    #     super().__init__(tap)
    #     if self._tap_state != {}:
    #         self._tap_state = self._tap_state['value']

    @property
    def url_base(self) -> str:
        return self.config["api_url"]

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        resp_json = response.json()['data']
        if isinstance(resp_json, dict):
            yield resp_json
        else:
            for row in resp_json:
                yield row

    @property
    def authenticator(self) -> APIAuthenticatorBase:
        encodedData = base64.b64encode(
            bytes(f"{self.config.get('username')}:{self.config.get('password')}", "UTF-8")).decode("ascii")
        http_headers = {"Authorization": f'Basic {encodedData}'}
        return SimpleAuthenticator(stream=self, auth_headers=http_headers)


class CustomerStream(s5Stream):

    def get_starting_timestamp(
            self, partition: Optional[dict]
    ) -> Optional[datetime.datetime]:
        state = self.get_stream_or_partition_state(partition)
        if self.config['use_state']:
            if self.is_timestamp_replication_key:
                if "replication_key_value" in state:
                    return pendulum.parse(state["replication_key_value"])
        else:
            if "start_date" in self.config:
                return pendulum.parse(self.config["start_date"])
        return None

    def get_url_params(
            self,
            partition: Optional[dict],
            next_page_token: Optional[Any] = None
    ) -> Dict[str, Any]:

        params = {}
        start_date = self.get_starting_timestamp(partition)
        if start_date is not None:
            params.update({"filter[0][property]": "changed",
                           "filter[0][expression]": ">",
                           "filter[0][value]": start_date
                           })
        params.update({"sort[0][property]": "changed"})
        params.update({"sort[0][direction]": "ASC"})
        return params

    name = "customers"
    path = "/customers"

    primary_keys = ['id']
    replication_key = 'changed'
    schema = PropertiesList(
        Property("id", IntegerType),
        Property("paymentId", IntegerType),
        Property("firstname", StringType),
        Property("firstLogin", DateTimeType),
        Property("changed", DateTimeType),
        Property("time_extracted", DateTimeType)
    ).to_dict()


class OrdersStream(s5Stream):

    def get_starting_timestamp(
            self, partition: Optional[dict]
    ) -> Optional[datetime.datetime]:
        """Return `start_date` config, or state if using timestamp replication."""
        state = self.get_stream_or_partition_state(partition)
        if self.config['use_state']:
            if self.is_timestamp_replication_key:
                if "replication_key_value" in state:
                    return pendulum.parse(state['replication_key_value'])
        else:
            if "start_date" in self.config:
                return pendulum.parse(self.config["start_date"])
        return None

    def get_url_params(
            self,
            partition: Optional[dict],
            next_page_token: Optional[Any] = None
    ) -> Dict[str, Any]:

        params = {}
        starting_datetime = self.get_starting_timestamp(partition)
        if starting_datetime is not None:
            params.update({"filter[0][property]": "orderTime",
                           "filter[0][expression]": ">",
                           "filter[0][value]": starting_datetime
                           })
        params.update({"sort[0][property]": "orderTime"})
        return params

    name = "OrdersSince"
    path = "/orders"
    # path = "/orders?filter[0][property]=orderTime&filter[0][expression]=>=&filter[0][value]={start_date}"

    primary_keys = ["id"]
    replication_key = 'orderTime'
    schema = PropertiesList(
        Property("customerId", IntegerType),
        Property("id", IntegerType),
        Property("orderTime", DateTimeType),
        Property("time_extracted", DateTimeType)
    ).to_dict()


class AddressesStream(s5Stream):

    def get_starting_timestamp(
            self, partition: Optional[dict]
    ) -> Optional[datetime.datetime]:
        """Return `start_date` config, or state if using timestamp replication."""
        state = self.get_stream_or_partition_state(partition)
        if self.config['use_state']:
            if "replication_key_value" in state:
                return state['replication_key_value']
        return None

    def get_url_params(
            self,
            partition: Optional[dict],
            next_page_token: Optional[Any] = None
    ) -> Dict[str, Any]:

        params = {}
        start_id = self.get_starting_timestamp(partition)
        if start_id is not None:
            params.update({"filter[0][property]": "address.id",
                           "filter[0][expression]": ">",
                           "filter[0][value]": start_id})
        params.update({"sort[0][property]": "address.id"})
        return params

    name = "addresses"
    path = "/addresses"

    primary_keys = ["id"]
    replication_key = 'id'
    schema = PropertiesList(
        Property("id", IntegerType),
        Property("customer", IntegerType),
        Property("street", StringType),
        Property("zipcode", StringType),
        Property("city", StringType)
    ).to_dict()


class ArticlesStream(s5Stream):

    def get_starting_timestamp(
            self, partition: Optional[dict]
    ) -> Optional[datetime.datetime]:
        state = self.get_stream_or_partition_state(partition)
        if self.config['use_state']:
            if self.is_timestamp_replication_key:
                if "replication_key_value" in state:
                    return pendulum.parse(state["replication_key_value"])
        else:
            if "start_date" in self.config:
                return pendulum.parse(self.config["start_date"])
        return None

    def get_url_params(
            self,
            partition: Optional[dict],
            next_page_token: Optional[Any] = None
    ) -> Dict[str, Any]:

        params = {}
        start_date = self.get_starting_timestamp(partition)
        if start_date is not None:
            params.update({"filter[0][property]": "changed",
                           "filter[0][expression]": ">",
                           "filter[0][value]": start_date
                           })
        params.update({"sort[0][property]": "changed"})
        params.update({"limit": "2000"})
        return params

    name = "articles"
    path = "/articles"

    primary_keys = ["id"]
    replication_key = 'changed'
    schema = PropertiesList(
        Property("id", IntegerType),
        Property("name", StringType),
        Property("changed", DateTimeType)
    ).to_dict()
